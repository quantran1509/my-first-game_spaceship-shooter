﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void BulletMove::Start()
extern void BulletMove_Start_m9FC56BD359347599403D218E5A1DE877736D3BD3 ();
// 0x00000002 System.Void BulletMove::Update()
extern void BulletMove_Update_mEBBD6D1E2F93EB2BDC41D35812783E5E97E13CEA ();
// 0x00000003 System.Void BulletMove::.ctor()
extern void BulletMove__ctor_m37987F31768DAC76E012A8E319E6C3712752DC73 ();
// 0x00000004 System.Void BGScroll::Start()
extern void BGScroll_Start_m5CB3432F9F44508FD6ECBBB81BD75E1CFA258230 ();
// 0x00000005 System.Void BGScroll::Update()
extern void BGScroll_Update_mC2158DA7F0679B858435E6C2B32E571574C35B2D ();
// 0x00000006 System.Void BGScroll::.ctor()
extern void BGScroll__ctor_mC047675654B4B43EF8508403E3A81E62D6B14F3A ();
// 0x00000007 System.Void Border::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Border_OnCollisionEnter2D_mE4BCE16C1CC50D716A84B55B69B6BD36C843F6AE ();
// 0x00000008 System.Void Border::.ctor()
extern void Border__ctor_m84BD25715A1392151B288EF3385FED3472EDEE84 ();
// 0x00000009 System.Void EnemyScript::Start()
extern void EnemyScript_Start_m47875901038E9AE8971E6221254D83E102CBF3B8 ();
// 0x0000000A System.Void EnemyScript::Update()
extern void EnemyScript_Update_m48E2D9202CE7C9AC58EE317D4730B3992527532F ();
// 0x0000000B System.Void EnemyScript::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void EnemyScript_OnCollisionEnter2D_mBA07168E94401C5305168653042F9C27E57C4D8C ();
// 0x0000000C System.Void EnemyScript::StartShooting()
extern void EnemyScript_StartShooting_mB052DAAB95214D104079CB16E09430C1FE979B6E ();
// 0x0000000D System.Void EnemyScript::DeactivateObject()
extern void EnemyScript_DeactivateObject_mEFCC1463760E5BAD4C8B8E645235E5E6579C5668 ();
// 0x0000000E System.Void EnemyScript::.ctor()
extern void EnemyScript__ctor_mD7AD8C15B2124634018267D133F20C71843C9FB4 ();
// 0x0000000F System.Void EnemySpawner::Start()
extern void EnemySpawner_Start_mC0F2354C1D2ED84F19D7BF22C89A2C198EBED26E ();
// 0x00000010 System.Void EnemySpawner::SpawnEnemy()
extern void EnemySpawner_SpawnEnemy_mE360CF09E227D236AF512863E657CCF85B3055FB ();
// 0x00000011 System.Void EnemySpawner::.ctor()
extern void EnemySpawner__ctor_m637C6372D629C685D64F22E5E15FD64F9F715F24 ();
// 0x00000012 System.Void EnemyBulletSpawn::Start()
extern void EnemyBulletSpawn_Start_mB85E7CC09CE1E091BC4334C2D71CBE9F0733F798 ();
// 0x00000013 System.Void EnemyBulletSpawn::SpawnEnemyBullet()
extern void EnemyBulletSpawn_SpawnEnemyBullet_m7B6DF04653FD58CECCB4538CD8958660BE9EA0EC ();
// 0x00000014 System.Void EnemyBulletSpawn::.ctor()
extern void EnemyBulletSpawn__ctor_mCDCCAE421C201473684F72C4C8455C8A6FEB86AC ();
// 0x00000015 System.Void LoadScene::OnClickButtonStart()
extern void LoadScene_OnClickButtonStart_m1326DCE4A202A93C798D0CBEF2EBD810DBFB8452 ();
// 0x00000016 System.Void LoadScene::.ctor()
extern void LoadScene__ctor_m52B221B2B1C02A68F125B164DDE573DF0353A336 ();
// 0x00000017 System.Void PauseButton::Start()
extern void PauseButton_Start_m4164AED4819B2E263BF28611E320B9D08562B02B ();
// 0x00000018 System.Void PauseButton::OnPauseButtonClick()
extern void PauseButton_OnPauseButtonClick_m6836BFF469D9A6918C09E7A040B23821D8CD66DC ();
// 0x00000019 System.Void PauseButton::.ctor()
extern void PauseButton__ctor_m7A650A70E93347F1B86A1FAC60A46F51D97D8664 ();
// 0x0000001A System.Void PlayerBoundaries::Start()
extern void PlayerBoundaries_Start_m008ABC33756F02DA0D9E443CDFD006E477F34714 ();
// 0x0000001B System.Void PlayerBoundaries::Update()
extern void PlayerBoundaries_Update_m421098E09F189EBCBA33A527272B8692B25BBDBF ();
// 0x0000001C System.Void PlayerBoundaries::.ctor()
extern void PlayerBoundaries__ctor_mD531B605005EF5457C5ED69013B461C443E386AE ();
// 0x0000001D System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_m729EA80FA27E8DE209E2D1887086DAA46DBF957C ();
// 0x0000001E System.Void PlayerMovement::FixedUpdate()
extern void PlayerMovement_FixedUpdate_m62744949B28CE83DD91D5457328C80D609A36ECD ();
// 0x0000001F System.Void PlayerMovement::Move()
extern void PlayerMovement_Move_m2E03B99A975764D9535595A219BA9023BBB8F19C ();
// 0x00000020 System.Void PlayerMovement::Fire()
extern void PlayerMovement_Fire_mDF770F892BEFE23DC2E0338574AE863F2E1EAAF8 ();
// 0x00000021 System.Void PlayerMovement::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PlayerMovement_OnCollisionEnter2D_mA50206117A660062034813C4AD4A91349B0141DF ();
// 0x00000022 System.Void PlayerMovement::RestartGame()
extern void PlayerMovement_RestartGame_m6CD93FCA6D090B12BB85F2863E7509FF74DBD29A ();
// 0x00000023 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_m8A0EF934ADA5029471AE677A0EB5A99E87442762 ();
// 0x00000024 System.Void Score::Start()
extern void Score_Start_m1821AC2C6C0505E9EEFA5DD52733BE5C3037AD9E ();
// 0x00000025 System.Void Score::Update()
extern void Score_Update_m034295E32383253E391ECFA2599FD3720345D1DB ();
// 0x00000026 System.Void Score::.ctor()
extern void Score__ctor_mE2F9D741565800994F2DFAF0C4036F5E7ADA4D1F ();
// 0x00000027 System.Void IconPreview::Awake()
extern void IconPreview_Awake_mE685BC50413C56166CAEFE8EA490C72548BDEDD3 ();
// 0x00000028 System.Void IconPreview::Update()
extern void IconPreview_Update_m3C0EE233ECB63E25426F652A91F4E9706E4FC7E1 ();
// 0x00000029 System.Void IconPreview::.ctor()
extern void IconPreview__ctor_mC79D660F599AC25566DE0598AFACE20B097E34F0 ();
static Il2CppMethodPointer s_methodPointers[41] = 
{
	BulletMove_Start_m9FC56BD359347599403D218E5A1DE877736D3BD3,
	BulletMove_Update_mEBBD6D1E2F93EB2BDC41D35812783E5E97E13CEA,
	BulletMove__ctor_m37987F31768DAC76E012A8E319E6C3712752DC73,
	BGScroll_Start_m5CB3432F9F44508FD6ECBBB81BD75E1CFA258230,
	BGScroll_Update_mC2158DA7F0679B858435E6C2B32E571574C35B2D,
	BGScroll__ctor_mC047675654B4B43EF8508403E3A81E62D6B14F3A,
	Border_OnCollisionEnter2D_mE4BCE16C1CC50D716A84B55B69B6BD36C843F6AE,
	Border__ctor_m84BD25715A1392151B288EF3385FED3472EDEE84,
	EnemyScript_Start_m47875901038E9AE8971E6221254D83E102CBF3B8,
	EnemyScript_Update_m48E2D9202CE7C9AC58EE317D4730B3992527532F,
	EnemyScript_OnCollisionEnter2D_mBA07168E94401C5305168653042F9C27E57C4D8C,
	EnemyScript_StartShooting_mB052DAAB95214D104079CB16E09430C1FE979B6E,
	EnemyScript_DeactivateObject_mEFCC1463760E5BAD4C8B8E645235E5E6579C5668,
	EnemyScript__ctor_mD7AD8C15B2124634018267D133F20C71843C9FB4,
	EnemySpawner_Start_mC0F2354C1D2ED84F19D7BF22C89A2C198EBED26E,
	EnemySpawner_SpawnEnemy_mE360CF09E227D236AF512863E657CCF85B3055FB,
	EnemySpawner__ctor_m637C6372D629C685D64F22E5E15FD64F9F715F24,
	EnemyBulletSpawn_Start_mB85E7CC09CE1E091BC4334C2D71CBE9F0733F798,
	EnemyBulletSpawn_SpawnEnemyBullet_m7B6DF04653FD58CECCB4538CD8958660BE9EA0EC,
	EnemyBulletSpawn__ctor_mCDCCAE421C201473684F72C4C8455C8A6FEB86AC,
	LoadScene_OnClickButtonStart_m1326DCE4A202A93C798D0CBEF2EBD810DBFB8452,
	LoadScene__ctor_m52B221B2B1C02A68F125B164DDE573DF0353A336,
	PauseButton_Start_m4164AED4819B2E263BF28611E320B9D08562B02B,
	PauseButton_OnPauseButtonClick_m6836BFF469D9A6918C09E7A040B23821D8CD66DC,
	PauseButton__ctor_m7A650A70E93347F1B86A1FAC60A46F51D97D8664,
	PlayerBoundaries_Start_m008ABC33756F02DA0D9E443CDFD006E477F34714,
	PlayerBoundaries_Update_m421098E09F189EBCBA33A527272B8692B25BBDBF,
	PlayerBoundaries__ctor_mD531B605005EF5457C5ED69013B461C443E386AE,
	PlayerMovement_Start_m729EA80FA27E8DE209E2D1887086DAA46DBF957C,
	PlayerMovement_FixedUpdate_m62744949B28CE83DD91D5457328C80D609A36ECD,
	PlayerMovement_Move_m2E03B99A975764D9535595A219BA9023BBB8F19C,
	PlayerMovement_Fire_mDF770F892BEFE23DC2E0338574AE863F2E1EAAF8,
	PlayerMovement_OnCollisionEnter2D_mA50206117A660062034813C4AD4A91349B0141DF,
	PlayerMovement_RestartGame_m6CD93FCA6D090B12BB85F2863E7509FF74DBD29A,
	PlayerMovement__ctor_m8A0EF934ADA5029471AE677A0EB5A99E87442762,
	Score_Start_m1821AC2C6C0505E9EEFA5DD52733BE5C3037AD9E,
	Score_Update_m034295E32383253E391ECFA2599FD3720345D1DB,
	Score__ctor_mE2F9D741565800994F2DFAF0C4036F5E7ADA4D1F,
	IconPreview_Awake_mE685BC50413C56166CAEFE8EA490C72548BDEDD3,
	IconPreview_Update_m3C0EE233ECB63E25426F652A91F4E9706E4FC7E1,
	IconPreview__ctor_mC79D660F599AC25566DE0598AFACE20B097E34F0,
};
static const int32_t s_InvokerIndices[41] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	41,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
