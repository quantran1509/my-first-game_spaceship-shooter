﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    public Text dearMyFriend;  
    private float timeToAppear = 2f;
    private float timeWhenDisappear;
    public static int scoreValue;
    public Text score;

    // Start is called before the first frame update
    void Start()
    {
        dearMyFriend.enabled = true;
        timeWhenDisappear = Time.time + timeToAppear;
        scoreValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (dearMyFriend.enabled && (Time.time >= timeWhenDisappear))
        {
            dearMyFriend.enabled = false;
        }
        score.text = "Score: " + scoreValue;
    }
}
