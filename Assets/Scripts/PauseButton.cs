﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : MonoBehaviour
{
    public GameObject paused;
    public GameObject gameOver;
    public PlayerMovement playerMovement;
    private void Start()
    {
        paused.SetActive(false);
    }
    public void OnPauseButtonClick()
    {

        if (Time.timeScale == 1)
        {
            
            Time.timeScale = 0;
            paused.SetActive(true);
            
        }
        else if (Time.timeScale == 0)
        {
            paused.SetActive(false);
            Time.timeScale = 1;
      
        }

    }
}
