﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private float delta_X , delta_Y;

    private AudioSource laseraudio;

    private PauseButton pauseButton;
    public float speed = 5f;
    public float offset = 1f;
    public float min_Y = -4.3f;
    public float max_Y = 4.3f;
    public bool canMove;
    public Transform firePoint;

    public WaitForSeconds yield;
    // Start is called before the first frame update
    void Start()
    {
        laseraudio = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("Fire", 2f, 0.5f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        if (Input.touchCount >0)
        {
            
               /* mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                direction = (mousePosition - transform.position).normalized;
                rb.velocity = new Vector2(direction.x * moveSpeed + offset, direction.y * moveSpeed);*/

                Touch touch = Input.GetTouch(0);
                Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

                switch(touch.phase)
                {
                    case TouchPhase.Began:
                        delta_X = touchPos.x - transform.position.x;
                        delta_Y = touchPos.y - transform.position.y;
                        break;
                    case TouchPhase.Moved:
                        rb.MovePosition(new Vector2(touchPos.x - delta_X, touchPos.y - delta_Y));
                        break;
                    case TouchPhase.Ended:
                        rb.velocity = Vector2.zero;
                        break;
                }
            
            

        }
        

    }

    void Fire()
    {
            GameObject bullet = ObjectPooler.Instance.GetPooledObjects();
            if (bullet != null)
            {
                bullet.transform.position = firePoint.transform.position;
                bullet.SetActive(true);
            }
                laseraudio.Play();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("EnemyBullet") || collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
            RestartGame();
        }
    }
    public void RestartGame()
    {
        SceneManager.LoadScene("StartMenu");
    }
}
