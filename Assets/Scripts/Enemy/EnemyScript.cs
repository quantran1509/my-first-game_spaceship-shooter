﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyScript : MonoBehaviour
{
    public float speed = 3f;
    public float rotateSpeed = 5f;
    public bool canRotate;
    public bool canMove;
    public bool canShoot;
    public Animator anim;

    public GameObject enemyBullet;
    public Transform firePoint;
    private AudioSource explosion;
    // Start is called before the first frame update
    void Start()
    {
        explosion = GetComponent<AudioSource>();
        if (canShoot)
        {
            InvokeRepeating("StartShooting", 1f, 1.5f);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            Vector2 temp = transform.position;
            temp.x -= speed * Time.deltaTime;
            transform.position = temp;
        }
        
       
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            Invoke("DeactivateObject",0.2f);
            anim.Play("Destroy");
            explosion.Play();
            canMove = false;
            canShoot = false;

            collision.gameObject.SetActive(false);
            Score.scoreValue++;
            CancelInvoke("StartShooting");
        }
    }
    public void StartShooting()
    {
        Instantiate(enemyBullet, firePoint.transform.position, Quaternion.identity);
    }
    public void DeactivateObject()
    {
        gameObject.SetActive(false);
    }
}
