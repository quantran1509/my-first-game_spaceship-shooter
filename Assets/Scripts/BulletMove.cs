﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    public float speed = 5f;
    public bool isPlayer;
    public bool isEnemy;

    private void OnEnable()
    {
        Invoke("Destroy", 3f);
    }

    void Update()
    {
        if (isPlayer)
        {
            Vector2 temp = transform.position;
            temp.x += speed * Time.deltaTime;
            transform.position = temp;
        }
        if (isEnemy)
        {
            Vector2 temp = transform.position;
            temp.x -= speed * Time.deltaTime;
            transform.position = temp;
        }
        
    }
    private void Destroy()
    {
        gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        CancelInvoke();
    }
}
